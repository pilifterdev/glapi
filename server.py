from cgitb import enable
from fastapi import FastAPI,Request
from api.v1.api import router as v1Router
app = FastAPI(title="GLAPI")

app.include_router(v1Router,prefix="/api/v1")



