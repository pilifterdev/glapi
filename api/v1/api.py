from fastapi import APIRouter

from .endpoints import competitions

router = APIRouter()
router.include_router(competitions.router,prefix="/competitions")