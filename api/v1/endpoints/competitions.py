from fastapi.security import HTTPBearer
from sqlalchemy.exc import SQLAlchemyError
from fastapi import APIRouter,Response,Depends
from sqlalchemy.orm import Session
import db,schemas,models
from datetime import datetime
from dependencies import validate_token,PermissionsValidator
router = APIRouter()

@router.get('/')
def getCompetitions(response:Response,status_code=200,db:Session = Depends(db.getDB))->list[schemas.CreatedCompetition]:
    return db.query(models.Competition).all()

@router.post("/",dependencies=[Depends(PermissionsValidator(['create:competitions']))])
def createCompetition(competition:schemas.Competition,response:Response,status_code=200,db:Session = Depends(db.getDB))->schemas.CreatedCompetition:
    newCompetition = models.Competition(name=competition.name,contactEmails=competition.contactEmails)
    for day in competition.competitionDays:
        newCompetition.competitionDays.append(models.CompetitionDay(location=day['location'],liftOffTime=datetime.fromisoformat(day['date']),places=day['places']))
    try:
        db.add(newCompetition)
        db.commit()
        db.refresh(newCompetition)
        return newCompetition
    except AssertionError as e:
        response.status_code = 422
        response.body = e
        return response

