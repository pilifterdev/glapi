from sqlalchemy import create_engine,MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,scoped_session
from sqlalchemy.pool import StaticPool
import os

convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

meta = MetaData(naming_convention=convention)

if os.getenv('ENV')=='test':
    SQLALCHEMY_DATABASE_URL = 'sqlite://'
else:
    SQLALCHEMY_DATABASE_URL = os.getenv('DATABASE_URL','sqlite://')

if 'sqlite' in SQLALCHEMY_DATABASE_URL:
    engine = create_engine(SQLALCHEMY_DATABASE_URL,connect_args={"check_same_thread": False},poolclass=StaticPool)
else:
    engine = create_engine(SQLALCHEMY_DATABASE_URL)

SessionLocal = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))

Base = declarative_base(metadata=meta)

def getDB():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

