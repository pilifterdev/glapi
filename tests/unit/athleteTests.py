import pytest
import sqlalchemy
from models import Athlete,Person
import categories

def testWhatAthleteRequires(testDB):
    assert testDB.query(Athlete).count()==0
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        person = Person(firstName="Dom",lastName="Patmore",email="xxx@gmail.com")
        athleteWithNoWeightClass = Athlete()
        athleteWithNoWeightClass.person = person
        testDB.add(athleteWithNoWeightClass)
        testDB.commit()
    testDB.rollback()

def testICanEnterAnAthlete(testDB):
    person = Person(firstName="Dom",lastName="Patmore",email="xxx@gmail.com")
    athleteWithNoWeightClass = Athlete(weightClass=categories.WeightCategories.M_U120)
    athleteWithNoWeightClass.person = person
    testDB.add(athleteWithNoWeightClass)
    testDB.commit()
    assert testDB.query(Person).count()==1
    assert testDB.query(Athlete).count()==1

