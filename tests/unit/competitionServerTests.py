from fastapi.testclient import TestClient
import sqlalchemy
from server import app
from dependencies import validate_token
import db
import models
from authorization_header_elements import get_bearer_token,get_authorization_header_elements
from fastapi import Response,Depends
from starlette.requests import Request
import custom_exceptions
from sqlalchemy import exists
testApp = TestClient(app)
async def validate_test_token(response:Response,token: str=Depends(get_bearer_token)):
    if token:
        if 'admin'  in token:
            return {'user':'admin',
                    'permissions':['create:competitions']
                    }
        else:
            return {'user':'user',
                    'permissions':[]
                    }
    else:
        raise custom_exceptions.BadCredentialsException



app.dependency_overrides.update({
    validate_token:validate_test_token,
    # db.getDB: db.fakeDB
})

def test_I_Get_A_401_If_I_Create_A_Competition_Without_A_Token():
    response = testApp.post('api/v1/competitions/',
                             json={"name":"Greater London Qualifiers","contactEmails":"good@email.com"})
    assert response.status_code==401

def test_I_Cannot_Create_A_Competition_Without_The_Right_Credentials():
    response = testApp.post('api/v1/competitions/',
                             headers={"Authorization":"Bearer some-user"},
                             json={"name":"Greater London Qualifiers","contactEmails":"good@email.com"})  
    
    assert response.status_code==403

def test_I_Can_Create_A_Competition_As_Admin():
    response = testApp.post('api/v1/competitions/',
                             headers={"Authorization":"Bearer admin-user"},
                             json={'name': 'Some',
                                    'contactEmails': 'some@email.com',
                                    'competitionDays': [
                                        {'location': 'Fort', 'places': '10', 'date': '2023-07-23T10:00'},
                                        {'location': 'Fort', 'places': '10', 'date': '2023-07-27T10:00'}
                                    ]
                                    }
                            )
    assert response.status_code == 200
    assert response.json()['name'] == "Some"
    assert len(response.json()['competitionDays']) == 2


def testThatIShouldGetANon200ResponseForACompetitionWithBadEmails():
    response = testApp.post('api/v1/competitions/',
                            headers={"Authorization":"Bearer admin-user"},
                            json={
                                "name":"Greater London Qualifiers",
                                "contactEmails":"good@email.com,email.com"
                                }
                            )
    assert response.status_code in range(400,499)
    assert "email address" in response.json()['detail'][0]['msg']

def testThatIShouldGetANon200ResponseForACompetitionWithNoName():
    response = testApp.post('api/v1/competitions/',
                            headers={"Authorization":"Bearer admin-user"},
                            json={"contactEmails":"good@email.com"}
                            )
    assert response.status_code in range(400,499)


def testIShouldBeAbleToGetCompetitions(testDB):
    testApp.post('api/v1/competitions/',
                             headers={"Authorization":"Bearer admin-user"},
                             json={'name': 'Some',
                                    'contactEmails': 'some@email.com',
                                    'competitionDays': [
                                        {'location': 'Fort', 'places': '10', 'date': '2023-07-23T10:00'},
                                        {'location': 'Fort', 'places': '10', 'date': '2023-07-27T10:00'}
                                    ]
                                    }
                            )
    response = testApp.get('api/v1/competitions/')
    assert response.status_code in range (200,299)
    competition = response.json()[0]
    assert 'competitionDays' in competition
    assert '23' in competition['startDate']
    assert '27' in competition['endDate']