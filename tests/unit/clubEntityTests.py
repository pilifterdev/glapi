from models.transactionHistory import TransactionHistory
import db
import pytest
import sqlalchemy
from models import (ClubEntity)


def testICanOnlyInsertWellBehavedClubsOrEntities(testDB):
    assert testDB.query(ClubEntity).count() == 0
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badClub2 = ClubEntity()
        testDB.add(badClub2)
        testDB.commit()
    testDB.rollback()
    goodClub = ClubEntity(name="My own club")
    testDB.add(goodClub)
    testDB.commit()
    assert testDB.query(ClubEntity).count() == 1

def testICanCorrectlySumPointsFromTransactions(testDB):
    club = testDB.query(ClubEntity).first()
    assert club.points==0
    club.transactions.append(TransactionHistory(event='sample insert',pointChange=10))
    club.transactions.append(TransactionHistory(event='another insert',pointChange=-6))
    testDB.commit()
    assert club.points == 4
    assert testDB.query(TransactionHistory).count()==2
