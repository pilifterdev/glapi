import pytest
import db

@pytest.fixture(scope="module",autouse=True)
def testDB():
    db.Base.metadata.create_all(bind=db.engine)
    session = db.SessionLocal()
    try:
        yield session
    finally:
        print("Dropping all tables")
        session.close()
        db.Base.metadata.drop_all(bind=db.engine)
        print("Database is clean")
    
