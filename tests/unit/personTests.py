import pytest
import sqlalchemy
from models import Person

def testPersonNeedsAFirstName(testDB):
    assert testDB.query(Person).count()==0
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badPersonNoFirstName = Person()
        testDB.add(badPersonNoFirstName)
        testDB.commit()
    testDB.rollback()

def testPersonNeedsALastName(testDB):
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badPersonNoLastName = Person(firstName="Dom")
        testDB.add(badPersonNoLastName)
        testDB.commit()
    testDB.rollback()

def testPersonNeedsAnEmail(testDB):
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badPersonNoEmail = Person(firstName="Dom",lastName="Patmore")
        testDB.add(badPersonNoEmail)
        testDB.commit()
    testDB.rollback()

def testPersonNeedsAnEmail(testDB):
    with pytest.raises(AssertionError):
        badPersonBadEmail = Person(firstName="Dom",lastName="Patmore",email="noeamil.com")
        testDB.add(badPersonBadEmail)
        testDB.commit()
    testDB.rollback()