import pytest
import sqlalchemy
from models import (Competition,competitionDay)


def testICanCreateACompetition(testDB):
    assert testDB.query(Competition).count() == 0
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badCompNoName = Competition()
        testDB.add(badCompNoName)
        testDB.commit() 
    testDB.rollback()






def testWhenICreateACompetitionTheStatusIsDraft(testDB):
    sampleComp = Competition(name="Greater London Qualifiers", contactEmails="good@email.com")
    testDB.add(sampleComp)
    testDB.commit()
    testDB.flush()
    testComp = testDB.query(Competition).first()
    assert testComp.status.name=='DRAFT'
    assert testComp.displayId is not None

