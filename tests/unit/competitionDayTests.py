from datetime import date, datetime
import pytest
import sqlalchemy
from models import (Person,CompetitionDay,Athlete)
from categories import WeightCategories
def testICanMakeACompetitionDay(testDB):
    assert testDB.query(CompetitionDay).count() == 0
    with pytest.raises(AssertionError):
        badCompInvalidMapURL = CompetitionDay(location="My House",mapURL="http://somewthing.weird")
        testDB.add(badCompInvalidMapURL)
        testDB.commit()
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badCompDayNoLocation = CompetitionDay()
        testDB.add(badCompDayNoLocation)
        testDB.commit()
    testDB.rollback()
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badCompNoPlaces = CompetitionDay(location="My House")
        testDB.add(badCompNoPlaces)
        testDB.commit()
    testDB.rollback()
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        badCompDayNoLiftOffTime = CompetitionDay(location="My House")
        testDB.add(badCompDayNoLiftOffTime)
        testDB.commit()
    testDB.rollback()

def testGettingWeighInTimesFromLiftOff(testDB):
    compDay = CompetitionDay(location="My house",liftOffTime=datetime(2022,1,16,10,00),places=1)
    testDB.add(compDay)
    testDB.commit()
    # retrieve comp
    comp = testDB.query(CompetitionDay).first()
    assert comp.suggestedWeighInStart().hour==8
    assert comp.suggestedWeighInEnd().hour==9
    assert comp.suggestedWeighInEnd().minute==30

def testCompDayHasPlacesForLifters(testDB):
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        compWithNoPlaces = CompetitionDay(location="My house",liftOffTime=datetime.now(),places=0)
        testDB.add(compWithNoPlaces)
        testDB.commit()
    testDB.rollback()

def testICanAddAnAthleteToACompetition(testDB):
    sampleComp = CompetitionDay(location="My Comp",liftOffTime=datetime.now(),places=1)
    person = Person(firstName="Dom",lastName="Patmore",email="dom@dompatmore.com")
    athlete= Athlete(weightClass=WeightCategories.M_U120)
    athlete.person = person
    sampleComp.athletes.append(athlete)
    testDB.add(sampleComp)
    testDB.add(athlete)
    testDB.commit()
    assert len(sampleComp.athletes)==1

def testICanComputeNumberOfPlacesRemaining(testDB):
    sampleComp = CompetitionDay(location="My Comp",liftOffTime=datetime.now(),places=10)
    person1 = Person(firstName="Dom",lastName="Patmore",email="dom1@dompatmore.com")
    athlete1= Athlete(weightClass=WeightCategories.M_U120)
    athlete1.person = person1
    person2 = Person(firstName="Domenica",lastName="Patmore",email="xxx@dompatmore.com")
    athlete2= Athlete(weightClass=WeightCategories.W_U43)
    athlete2.person = person2
    sampleComp.athletes.append(athlete1)
    sampleComp.athletes.append(athlete2)
    testDB.add(sampleComp)
    # testDB.add(athlete)
    testDB.commit()
    assert len(sampleComp.athletes)==2
    assert sampleComp.placesRemaining()==8
