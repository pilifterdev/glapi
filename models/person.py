from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.orm import column_property, validates,relationship

from db import Base

class Person(Base):
    __tablename__ = 'people'
    id = Column(Integer,primary_key=True)
    firstName = Column(String,nullable=False)
    lastName = Column(String,nullable=False)
    email = Column(String,nullable=False,unique=True)
    fullName = column_property(firstName+" "+lastName)

    @validates('email')
    def checkEmail(self, key, email):
        assert "@" in email, "Invalid email"
        return email