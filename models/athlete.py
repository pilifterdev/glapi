from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.orm import column_property, validates,relationship
from sqlalchemy.sql.schema import ForeignKey

from db import Base
import categories
from models import competitionDay

class Athlete(Base):
    __tablename__ = 'athletes'
    id = Column(Integer,primary_key=True)
    personId = Column(Integer,ForeignKey("people.id"))
    person = relationship("Person")
    weightClass = Column(Enum(categories.WeightCategories),nullable=False)
    competitionDay = Column(Integer,ForeignKey("competition_day.id"))