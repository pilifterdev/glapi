from datetime import datetime,timedelta
from email.policy import default
from os import name
from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.orm import validates,relationship,column_property
from sqlalchemy.sql.expression import null, or_
from sqlalchemy.sql.schema import CheckConstraint, ForeignKey
from sqlalchemy.sql.sqltypes import DateTime
from sqlalchemy.ext.hybrid import hybrid_property,hybrid_method


from db import Base



class CompetitionDay(Base):
    __tablename__ = 'competition_day'
    __table_args__ = (
        CheckConstraint('places > 0',name="initial places available"),
    )
    id = Column(Integer, primary_key=True)
    location = Column(String,nullable=False)
    mapURL = Column(String)
    liftOffTime=Column(DateTime,nullable=False)
    weighInStart = Column(DateTime)
    weighInEnd = Column(DateTime)
    additionalInfo = Column(String)
    places = Column(Integer, nullable=False)
    competitionId = Column(Integer,ForeignKey("competitions.id"))
    competition = relationship("Competition",back_populates="competitionDays")

    athletes = relationship("Athlete")

    @hybrid_method
    def placesRemaining(self):
        return self.places-len(self.athletes)
    @hybrid_method
    def suggestedWeighInStart(self):
        if self.weighInStart:
            return self.weighInStart
        else:
            return self.liftOffTime - timedelta(hours=2)
    @hybrid_method
    def suggestedWeighInEnd(self):
        if self.weighInEnd:
            return self.weighInEnd
        else:
            return self.liftOffTime - timedelta(minutes=30)
    @validates('mapURL')
    def checkURL(self, key, mapURL):
        assert 'https://' in mapURL, "Invalid map URL"
        return mapURL


