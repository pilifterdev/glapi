import datetime
from email.policy import default
from uuid import UUID
import uuid
from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.ext.hybrid import hybrid_property,hybrid_method
from sqlalchemy.orm import validates,relationship
from sqlalchemy.dialects.postgresql import UUID
from fastapi_restful.guid_type import GUID,GUID_DEFAULT_SQLITE,GUID_SERVER_DEFAULT_POSTGRESQL
import enum

from sqlalchemy.sql.schema import CheckConstraint
from db import Base

class CompetitionStatus(enum.Enum):
    DRAFT = 'created'
    OPEN = 'registration open'
    CLOSED = 'registration closed'
    COMPLETED = 'competition complete'

class Competition(Base):
    __tablename__ = 'competitions'

    id = Column(Integer, primary_key=True)
    displayId = Column(GUID,default=uuid.uuid4,unique =True, nullable=False)
    name = Column(String,nullable=False)
    status = Column(Enum(CompetitionStatus),default=CompetitionStatus.DRAFT)
    contactEmails = Column(String, nullable=False)
    competitionDays = relationship("CompetitionDay",back_populates="competition",lazy="selectin")

    def sortedCompetitionDays(self)->list:
        sortedDays = sorted(self.competitionDays,key=lambda x:x.liftOffTime)
        return sortedDays
   
    @hybrid_property
    def startDate(self)->datetime.datetime:
        return self.sortedCompetitionDays()[0].liftOffTime
    
    @hybrid_property
    def endDate(self)->datetime.datetime:
        return self.sortedCompetitionDays()[-1].liftOffTime



