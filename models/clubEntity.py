from sqlalchemy import Column,Integer,String,Enum,Float
from sqlalchemy.orm import validates,relationship
from sqlalchemy.ext.hybrid import hybrid_property
import enum
from db import Base

class ClubType(enum.Enum):
    INDIVIDUAL = "Individual"
    CLUB = "Club"

class ClubStatus(enum.Enum):
    ACTIVE = "Active"
    DORMANT =  "Dormant"

class ClubEntity(Base):
    __tablename__ = 'club_entities'

    id = Column(Integer, primary_key=True)
    name = Column(String,unique=True,nullable=False)
    type = Column(Enum(ClubType),default="CLUB")
    status = Column(Enum(ClubStatus),default="ACTIVE")


    transactions = relationship("TransactionHistory",back_populates="club")
    @hybrid_property
    def points(self):
        return sum([transaction.pointChange for transaction in self.transactions])

