from datetime import datetime
import email
from multiprocessing.sharedctypes import Value
from typing import List
import uuid
from wsgiref.validate import validator
from xml.dom import ValidationErr
from pydantic import BaseModel,validator,conlist
from models.competition import CompetitionStatus
from models.competitionDay import CompetitionDay
class Competition(BaseModel):
    name:str
    contactEmails: str
    competitionDays:list[dict]

    @validator("contactEmails")
    def checkEmails(cls,v):
        emails = v.split(",")
        for email in emails:
            if '@' not in email:
                raise ValueError("Invalid email address")
        return v



class CreatedCompetition(BaseModel):
    displayId:uuid.UUID
    name:str
    startDate:datetime
    endDate:datetime
    status:CompetitionStatus
    competitionDays: List

    @validator("competitionDays")
    def checkCompetitionDays(cls,v):
        if len(v) < 1:
            raise ValidationErr("A competition needs to have at least 1 day assigned")
        return v
    class Config:
        orm_mode = True
        use_enum_values = True

